const path = require('path');
const sveltePreprocess = require('svelte-preprocess');

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx|svelte)"
  ],
  "addons": [
    '@storybook/addon-a11y',
    '@storybook/preset-scss',
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-svelte-csf"
  ],
  webpackFinal: (config) => {
    // fix for storybook webpack5 + svelte
    config.resolve.alias.svelte = path.resolve('node_modules', 'svelte')

    // resolve sass inside component style tag
    // this will break the storybook's ability auto infer the props
    const svelteLoader = config.module.rules.find(r => r.loader && r.loader.includes('svelte-loader'));
    svelteLoader.options = {
      preprocess: sveltePreprocess()
    };
    
    return config;
  },
  core: {
    builder: 'webpack5',
  },
}