import List from "./List.svelte";
import WrapperListWithSlot from './WrapperListWithSlot.svelte';
import type { InterfaceWrapperItem } from './interfaces'

export default {
    title: 'Components/List',
    component: List,
    argTypes: {
    }
}

const TemplateWithSlot = (_args) => {
    const ret = ({ ...props }) => ({
        Component: WrapperListWithSlot,
        props
    })
    ret.args = _args
    return ret
}

const TemplateWithoutSlot = (_args) => {
    const ret = ({ ...props }) => ({
        Component: List,
        props
    })
    ret.args = _args
    return ret
}

const defaultItemsList :InterfaceWrapperItem[] = [
    {
        label: 'item 1',
        data: { icon: 'code' },
        children: [
            { label: 'item 1.1', data: { icon: 'comments' }},
            { label: 'item 1.2', data: { icon: 'comments' }},
            { label: 'item 1.3', data: { icon: 'comments' }},
        ]
    },
    {
        label: 'item 2',
        data: { icon: 'comments' },
    },
    {
        label: 'item 3',
        data: { icon: 'comments' },
    },
    {
        label: 'item 4',
        data: { icon: 'comments' },
    },
    {
        label: 'item 5',
        data: { icon: 'code' },
        children: [
            { label: 'item 5.1', data: { icon: 'comments' }},
            { label: 'item 5.2', data: { icon: 'comments' }},
            {
                label: 'item 5.3',
                data: { icon: 'code' },
                children: [
                    { label: 'item 5.3.1', data: { icon: 'comments' }},
                    { label: 'item 5.3.2', data: { icon: 'comments' }},        
                ]
            },
        ]
    },
]

export const WithoutItemSlot = TemplateWithoutSlot({
    items: defaultItemsList
})

export const WithItemSlot = TemplateWithSlot({
    items: defaultItemsList
})