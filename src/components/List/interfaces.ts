export interface InterfaceItem {
    label: string,
    children?: Array<InterfaceItem>,
    data?: any
}

export interface InterfaceWrapperItem extends InterfaceItem {
    data: {
        icon: 'code' | 'comments' | 'plugin'
    },
    children?: Array<InterfaceWrapperItem>,
}