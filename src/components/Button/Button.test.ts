import {render} from '@testing-library/svelte'

import ButtonWrapperForTestSlot from './ButtonWrapperForTestSlot.svelte'

test('show button with label', () => {
    const { getByRole } = render(ButtonWrapperForTestSlot, {
        labelSlot: 'my-button'
    })

    const button = getByRole('button')
    expect(button).toBeTruthy()
    expect(button).toHaveTextContent('my-button')
})